packer {
  required_plugins {
    hyperv = {
      version = ">= 1.0.4"
      source  = "github.com/hashicorp/hyperv"
    }
    ansible = {
      version = ">= 1"
      source  = "github.com/hashicorp/ansible"      
    }
  }
}

variable "boot_command" {
  type    = list(string)
  default = [""]
}

variable "disk_size" {
  type    = string
  default = ""
}

variable "memory" {
  type    = string
  default = ""
}

variable "cpus" {
  type    = string
  default = ""
}

variable "iso_checksum" {
  type    = string
  default = ""
}

variable "iso_checksum_type" {
  type    = string
  default = "none"
}

variable "iso_url" {
  type    = string
  default = ""
}

variable "output_directory" {
  type    = string
  default = ""
}

variable "ssh_password" {
  type    = string
  default = ""
  sensitive = true
}

variable "ssh_username" {
  type    = string
  default = ""
  sensitive = true
}

variable "switch_name" {
  type    = string
  default = ""
}

variable "vlan_id" {
  type    = string
  default = ""
}

variable "vm_name" {
  type    = string
  default = ""
}

variable "http_directory" {
  type    = string
  default = ""
}

variable "temp_path" {
  type    = string
  default = ""
}

variable "boot_wait" {
  type    = string
  default = "5s"
}

variable "communicator" {
  type    = string
  default = "ssh"
}

variable "disk_block_size" {
  type    = string
  default = "1"
}

variable "enable_dynamic_memory" {
  type    = string
  default = "true"
}

variable "enable_secure_boot" {
  type    = bool
  default = false
}

variable "generation" {
  type    = number
  default = 2
}

variable "guest_additions_mode" {
  type    = string
  default = "disable"
}

variable "shutdown_command" {
  type    = string
  default = "echo 'password' | sudo -S shutdown -P now"
}

variable "shutdown_timeout" {
  type    = string
  default = "15m"
}

variable "ssh_timeout" {
  type    = string
  default = "4h"
}

variable "path_copy_file" {
  type    = string
  default = ""
}

variable "script_shell_path" {
  type    = string
  default = ""
}

variable "inline_shell_command" {
  type    = list(string)
  default = [""]
}

variable "ansible_playbook_file_path" {
  type    = string
  default = ""
}

source "hyperv-iso" "vm" {
  boot_command          = "${var.boot_command}"
  boot_wait             = "${var.boot_wait}"
  communicator          = "${var.communicator}"
  cpus                  = "${var.cpus}"
  disk_block_size       = "${var.disk_block_size}"
  disk_size             = "${var.disk_size}"
  enable_dynamic_memory = "${var.enable_dynamic_memory}"
  enable_secure_boot    = "${var.enable_secure_boot}"
  generation            = "${var.generation}"
  guest_additions_mode  = "${var.guest_additions_mode}"
  http_directory        = "${var.http_directory}"
  iso_checksum          = "${var.iso_checksum_type}:${var.iso_checksum}"
  iso_url               = "${var.iso_url}"
  memory                = "${var.memory}"
  output_directory      = "${var.output_directory}"
  shutdown_command      = "${var.shutdown_command}"
  shutdown_timeout      = "${var.shutdown_timeout}"
  ssh_password          = "${var.ssh_password}"
  ssh_timeout           = "${var.ssh_timeout }"
  ssh_username          = "${var.ssh_username}"
  switch_name           = "${var.switch_name}"
  temp_path             = "${var.temp_path}"
  vlan_id               = "${var.vlan_id}"
  vm_name               = "${var.vm_name}"
}

build {
  sources = ["source.hyperv-iso.vm"]

  #Copy prepared files 
  provisioner "file" {
    destination = "/tmp/"
    source      = "${var.path_copy_file}"
  }

  #Inline shell commands
  provisioner "shell" {
    execute_command   = "chmod +x {{ .Path }}; {{ .Vars }} sudo -E sh '{{ .Path }}'"
    expect_disconnect = true
    inline            = "${var.inline_shell_command}"
    inline_shebang    = "/bin/sh -x"
  }

  #Script shell commands
  provisioner "shell" {
    script       = "${var.script_shell_path}"
    pause_before = "10s"
    timeout      = "10s"
  }

  #Ansible playbook file
#  provisioner "ansible" {
#    playbook_file = "${var.ansible_playbook_file_path}"
#  }

}
