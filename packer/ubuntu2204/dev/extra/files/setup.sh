#!/bin/bash

# Disable cloud-init
echo '> Cleaning cloud-init ...'
touch /etc/cloud/cloud-init.disabled

# Cleaning cloud-init
echo '> Cleaning cloud-init ...'
rm -rf /etc/cloud/cloud.cfg.d/subiquity-disable-cloudinit-networking.cfg
rm -rf /etc/cloud/cloud.cfg.d/99-installer.cfg
/usr/bin/cloud-init clean --logs --seed

# Cleaning all audit logs
echo '> Cleaning all audit logs ...'
if [ -f /var/log/audit/audit.log ]; then
cat /dev/null > /var/log/audit/audit.log
fi
if [ -f /var/log/wtmp ]; then
cat /dev/null > /var/log/wtmp
fi
if [ -f /var/log/lastlog ]; then
cat /dev/null > /var/log/lastlog
fi

# Cleans the machine-id
echo '> Cleaning the machine-id ...'
truncate -s 0 /etc/machine-id
rm /var/lib/dbus/machine-id
ln -s /etc/machine-id /var/lib/dbus/machine-id

# Cleans apt-get
echo '> Cleaning apt-get ...'
apt-get clean
apt clean

# Uninstall snap
sudo snap remove lxd && snap list | snap list | grep -v "^Name" | awk '{ system("sudo snap remove " $1) }'
sudo umount /run/snap/ns
sudo systemctl disable snapd.service snapd.socket snapd.seeded.service snapd.autoimport.service snapd.apparmor.service
sudo rm -rf /etc/apparmor.d/usr.lib.snapd.snap-confine.real
sudo systemctl start apparmor.service
df | grep snap | awk '{ system("sudo umount " $6) }'
sudo apt purge snapd -y
sudo rm -rf ~/snap /snap /var/snap /root/snap
sudo find / -name "*snapd*" | while read i; do echo $i; sudo rm -rf "$i"; done

# Install Docker
echo '> Docker install ...'
echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
apt-get update
apt-get upgrade -y
apt-get install apt-transport-https ca-certificates curl software-properties-common -y -q
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
cp /usr/share/keyrings/docker-archive-keyring.gpg /etc/apt/trusted.gpg.d/
apt-get update
apt-get install docker-ce -y -q
systemctl restart docker.service
usermod -aG docker ${USER}
sysctl net.bridge.bridge-nf-call-iptables=1

# Install Ansible
echo '> Ansible install ...'
echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
apt-add-repository ppa:ansible/ansible
apt update
apt install ansible -y