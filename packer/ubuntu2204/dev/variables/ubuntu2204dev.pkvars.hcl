ssh_password="root"
ssh_username="root"

switch_name="dmzSwitch"
vlan_id=""
vm_name="packer-preparing-ubuntu2204dev"

memory="1524"
cpus="1"
disk_size="50000"
#disk_additional_size=["10000"]

iso_checksum_type="sha256"
iso_checksum="84aeaf7823c8c61baa0ae862d0a06b03409394800000b3235854a6b38eb4856f"

http_directory="./ubuntu2204/dev/extra/files"
iso_url="./ubuntu2204/ubuntu2204-live-server-amd64.iso"
output_directory="./ubuntu2204/dev/complete-ubuntu2204dev"
temp_path="./ubuntu2204/dev"

boot_command=[
        "<esc><esc><esc><esc>e<wait>",
        "<del><del><del><del><del><del><del><del>",
        "<del><del><del><del><del><del><del><del>",
        "<del><del><del><del><del><del><del><del>",
        "<del><del><del><del><del><del><del><del>",
        "<del><del><del><del><del><del><del><del>",
        "<del><del><del><del><del><del><del><del>",
        "<del><del><del><del><del><del><del><del>",
        "<del><del><del><del><del><del><del><del>",
        "<del><del><del><del><del><del><del><del>",
        "<del><del><del><del><del><del><del><del>",
        "<del><del><del><del><del><del><del><del>",
        "<del><del><del><del><del><del><del><del>",
        "<del><del><del><del><del><del><del><del>",
        "<del><del><del><del><del><del><del><del>",
        "<del><del><del><del><del><del><del><del>",
        "linux /casper/vmlinuz --- autoinstall ds=\"nocloud-net;seedfrom=http://{{.HTTPIP}}:{{.HTTPPort}}/\"<enter><wait>",
        "initrd /casper/initrd<enter><wait>",
        "boot","<enter>",
        "<enter><f10><wait>"
      ]
      
path_copy_file="./ubuntu2204/dev/extra/files/uefi.sh"
script_shell_path="./ubuntu2204/dev/extra/files/setup.sh"
inline_shell_command=[
      "chmod +x /tmp/uefi.sh",
      "/tmp/uefi.sh",
      "sync;sync;reboot"
      ]
ansible_playbook_file_path="./ubuntu2204/dev/extra/files/playbook/*.yml"