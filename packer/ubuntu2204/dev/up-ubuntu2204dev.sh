#!/usr/bin/env bash

#src dir
#cd "$cidirhypervpacker"

# Get Start Time
export startDTM="$(date)"

# Variables
export path="$(pwd)"
export template_file="$path/ubuntu2204/ubuntu-template-hyperv.pkr.hcl"
export var_file="$path/ubuntu2204/dev/variables/ubuntu2204dev.pkvars.hcl"
export machine="packer-preparing-ubuntu2204dev"
export packer_log=0

if (( $template_file -r && $var_file -r )); then
echo '> Template and Var file found'
echo '> Building: '$machine''
packer validate -var-file=$var_file $template_file
else
echo '> Packer validation failed, exiting.'
fi

if (( $template_file -r && $var_file -r )); then
packer validate -var-file=$var_file $template_file
else
echo '> Packer build failed, exiting.'
fi

endDTM=$(date)
echo '>>> [INFO] - Elapsed Time: '$startDTM' - '$endDTM''
