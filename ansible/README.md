## Just For Your INFO

### _Prep_
### Отредактировать 4 файла
**Мини-коммент:** \
 Внимание на переменные, не удалите важные кавычки \
.cfg подойдет

- inventory 
  * $(node user), $(ip/FQDN), $(path to private_key)

- provision-manage.yml
  * required: $(node-manage user) x1, $(your pass) x2 
  * non-required: $(your name) x3 

- provision-export.yml
  * $(node-export user) x2

- ./stack/stack-manage/prometheus/prometheus.yml
  * $(node-export-ip/FQDN) x4

_**Зачистка докера**_ \
docker stop $(docker ps -a -q) \
docker rm $(docker ps -a -q) \
docker system prune -a \
docker system prune --volumes \
sudo apt-get purge docker-buildx-plugin docker-ce docker-ce-cli docker-ce-rootless-extras docker-compose-plugin docker-compose docker containerd.io

_**ГУИ портейнер**_ \
sudo docker run -d --network=host   -p 9443:9443   --name portainer   --restart unless-stopped   -v data:/data   -v /var/run/docker.sock:/var/run/docker.sock   portainer/portainer-ce:latest

_**user_data Первичная подготовка хоста после развертывания из ubuntu22.04-server-net**_ \
$(your username) x5 \
$(your hostname) x1 \
$(mkpasswd --method=SHA-512 --rounds=4096) x2 \
$(your public_key) x1
* hard fix Kernel \
sudo sysctl net.bridge.bridge-nf-call-iptables=1
* hard pull DNS \
echo "DNS=127.0.0.1 #$(your dns)" | sudo tee -a /etc/systemd/resolved.conf

##### Хотелось написать еще много всего, но перехотелось








